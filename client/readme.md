Open the folder in your command line, and install the needed dependencies:

composer install
cd client
npm install
bower install

Finally, run `foundation watch` to run Gulp. Your finished site will be created in a folder called `web`,

To create compressed, production-ready assets, run `npm run build`.
