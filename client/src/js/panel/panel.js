let panelApp = angular.module('PanelApp', [
    'pascalprecht.translate',
    'ngSanitize',
    'ngCookies'
]);

panelApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

panelApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.cache = true;
}]);

panelApp.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.translations('en', translationsEN);
    $translateProvider.preferredLanguage('en');
    $translateProvider.useCookieStorage();
}]);

panelApp.run(['$rootScope', '$translate', '$location', '$cookieStore', '$http', function ($rootScope, $translate, $location, $cookieStore, $http) {
    // keep user logged in after page refresh
}]);