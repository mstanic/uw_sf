panelApp.controller('PanelController',  function ($scope, $http, $translate) {

    $scope.messages = {};

    function loadUserInfo() {
        $http.get('panel/' + $translate.use()).then(function (response) {
            $scope.data = response['data'];
        });
    }

    $scope.currentIndex = 0; // Initially the index is at the first message

    loadUserInfo();
});