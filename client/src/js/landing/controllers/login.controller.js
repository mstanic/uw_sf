landingApp.controller('LoginController', function ($rootScope, $scope, $http) {

    $scope.error = false;

    $scope.login = function (username, password) {
        let hash;

        hash = CryptoJS.SHA3(password, {outputLength: 512});
        password = hash.toString();

        $http.post('/authenticate', {
            username: username,
            password: password
        })
        .then(function (response) {
            if (response['data'] == 201) {
                window.top.location.href = "/";
            } else {
                $scope.error = true;
            }
        });
    }

});