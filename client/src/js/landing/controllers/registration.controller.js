landingApp.controller('RegistrationController', function ($rootScope, $scope, $http) {

    $scope.user_locale = window.user_locale;
    $scope.user_token = window.user_token;

    $scope.registered = 0;
    $scope.error = false;
    $scope.user = {};

    $scope.register = function () {
        $scope.user.password_hash = CryptoJS.SHA3($scope.user.password, {outputLength: 512}).toString();
        $scope.user.confirm_password_hash = CryptoJS.SHA3($scope.user.confirm_password, {outputLength: 512}).toString();

        $scope.user.password = '';
        $scope.user.confirm_password = '';

        $scope.user.recaptcha = grecaptcha.getResponse();

        $http.post('/register', $scope.user)
            .then(function (response) {
                if (response['data'] == 204) {
                    $scope.registered = 1;
                    $rootScope.showConfirmation();
                } else {
                    let $modal = $('#modal').foundation();
                    $modal.foundation('open');
                }
            });
    };
});