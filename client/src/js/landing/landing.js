let landingApp = angular.module('LandingApp', [
    'pascalprecht.translate',
    'ngSanitize',
    'ngCookies'
]);

landingApp.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'https://*.domain.com/**']);
});

landingApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

landingApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.cache = true;
}]);

landingApp.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.translations('en', translationsEN);
    $translateProvider.preferredLanguage('en');
    $translateProvider.useCookieStorage();
}]);

landingApp.run(['$rootScope', '$location', '$cookieStore', '$http', '$translate', function ($rootScope, $location, $cookieStore, $http, $translate) {

    $rootScope.lang = 'en';

    $rootScope.landingFlag = false;
    $rootScope.loginFlag = true;
    $rootScope.registrationFlag = true;
    $rootScope.passwordResetFlag = true;
    $rootScope.passwordSetFlag = true;
    $rootScope.confirmationFlag = true;

    $rootScope.showLanding = function () {
        $rootScope.landingFlag = false;
        $rootScope.loginFlag = true;
        $rootScope.registrationFlag = true;
        $rootScope.spBrowserFlag = true;
        $rootScope.passwordResetFlag = true;
        $rootScope.passwordSetFlag = true;
        $rootScope.confirmationFlag = true;
    };

    $rootScope.showLogin = function () {
        $rootScope.landingFlag = true;
        $rootScope.loginFlag = false;
        $rootScope.registrationFlag = true;
        $rootScope.passwordResetFlag = true;
        $rootScope.passwordSetFlag = true;
        $rootScope.confirmationFlag = true;
    };

    $rootScope.showRegistration = function () {
        $rootScope.landingFlag = true;
        $rootScope.loginFlag = true;
        $rootScope.registrationFlag = false;
        $rootScope.spBrowserFlag = true;
        $rootScope.passwordResetFlag = true;
        $rootScope.passwordSetFlag = true;
        $rootScope.confirmationFlag = true;
    };

    $rootScope.showPasswordReset = function () {
        $rootScope.landingFlag = true;
        $rootScope.loginFlag = true;
        $rootScope.registrationFlag = true;
        $rootScope.passwordResetFlag = false;
        $rootScope.passwordSetFlag = true;
        $rootScope.confirmationFlag = true;
    };

    $rootScope.showPasswordSet = function () {
        $rootScope.landingFlag = true;
        $rootScope.loginFlag = true;
        $rootScope.registrationFlag = true;
        $rootScope.passwordResetFlag = true;
        $rootScope.passwordSetFlag = false;
        $rootScope.confirmationFlag = true;
    };

    $rootScope.showConfirmation = function () {
        $rootScope.landingFlag = true;
        $rootScope.loginFlag = true;
        $rootScope.registrationFlag = true;
        $rootScope.passwordResetFlag = true;
        $rootScope.passwordSetFlag = true;
        $rootScope.confirmationFlag = false;
    };
}]);