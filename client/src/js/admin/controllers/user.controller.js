adminApp.controller('AdminController',  function ($scope, $http, $translate) {

    $scope.messages = {};

    function loadUserInfo() {
        $http.get('admin/' + $translate.use()).then(function (response) {
            $scope.data = response['data'];

            $scope.message_count = $scope.data.message_count;

            if ($scope.message_count == 0) {
                $scope.currentIndex = -1;
            } else {
                $scope.currentIndex = 0;
            }

            if ($scope.data.messages) {
                $scope.messages = $scope.data.messages;
                $scope.messages[0].visible = true; // make the current message visible
            }
        });
    }

    $scope.currentIndex = 0; // Initially the index is at the first message

    if ($scope.message_count == 0) {
        $scope.currentIndex = -1;
    } else {
        $scope.currentIndex = 0;
    }

    $scope.next = function () {
        $scope.messages[$scope.currentIndex].visible = false;
        $scope.currentIndex < $scope.data.message_count - 1 ? $scope.currentIndex++ : $scope.currentIndex = 0;
        $scope.messages[$scope.currentIndex].visible = true;
    };

    $scope.prev = function () {
        $scope.messages[$scope.currentIndex].visible = false;
        $scope.currentIndex > 0 ? $scope.currentIndex-- : $scope.currentIndex = $scope.data.message_count - 1;
        $scope.messages[$scope.currentIndex].visible = true;
    };

    loadUserInfo();
});