let adminApp = angular.module('AdminApp', [
    'pascalprecht.translate',
    'ngSanitize',
    'ngCookies'
]);

adminApp.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

adminApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.cache = true;
}]);

adminApp.config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy('escape');
    $translateProvider.translations('en', translationsEN);
    $translateProvider.preferredLanguage('en');
    $translateProvider.useCookieStorage();
}]);

adminApp.run(['$rootScope', '$translate', '$location', '$cookieStore', '$http', function ($rootScope, $translate, $location, $cookieStore, $http) {
    // keep user logged in after page refresh

    $rootScope.newFlag = false;
    $rootScope.allFlag = true;

    $rootScope.showNew = function () {
        $rootScope.newFlag = false;
        $rootScope.allFlag = true;
    };

    $rootScope.showAll = function () {
        $rootScope.newFlag = true;
        $rootScope.allFlag = false;
    };

    $rootScope.slide1Flag = false;
    $rootScope.slide2Flag = true;
    $rootScope.slide3Flag = true;

    $rootScope.showSlide1 = function () {
        $rootScope.slide1Flag = false;
        $rootScope.slide2Flag = true;
        $rootScope.slide3Flag = true;
    };

    $rootScope.showSlide2 = function () {
        $rootScope.slide1Flag = true;
        $rootScope.slide2Flag = false;
        $rootScope.slide3Flag = true;
    };

    $rootScope.showSlide3 = function () {
        $rootScope.slide1Flag = true;
        $rootScope.slide2Flag = true;
        $rootScope.slide3Flag = false;
    };
}]);