let translationsEN = {
    'USERNAME': 'Username',
    'PASSWORD': 'Password',

    'USER_INFO': 'User information',
    'PERSONAL_INFO': 'Personal information',
    'CONTACT_INFO': 'Contact information',
    'TERMS_INFO': 'Terms and conditions',
    'ACCEPT': 'I accept terms and conditions',

    'CONFIRM_PASSWORD': 'Confirm password',
    'DOCUMENT_COUNTRY': 'Issuing country',
    'DOCUMENT_TYPE': 'Document type',
    'DOCUMENT_NUMBER': 'Document number',

    'LOGIN': 'Login',
    'LOGOUT': 'Logout',
    'REGISTRATION': 'Register',
    'REGISTER': 'Register',
    'PASSWORD_RESET': 'Forgot password?',
    'RESET': 'Reset',
    'CANCEL': 'Cancel',
    'REGISTER_CONFIRM': 'A confirmation link has been sent to your e-mail address.',
    'REGISTER_ACTIVATED': 'Your account has been activated. You can login by clicking on the button below.',

    // Obavijesti
    'NOTIFICATIONS': 'Notifications',

    // About
    'ABOUT': 'Personal information',
    'NAME': 'Name',
    'SURNAME': 'Surname',

    // Contact information
    'CONTACT': 'Contact information',
    'PHONE': 'Mobile',
    'EMAIL': 'e-mail',

    // Misc
    'NO_RESULTS': 'No results',
    'NEW_QUESTION': 'New question',
    'WRONG:USERNAME': 'Wrong username',

    'PDF': 'Download PDF'
};