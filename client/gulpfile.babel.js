'use strict';

import plugins  from 'gulp-load-plugins';
import yargs    from 'yargs';
import gulp     from 'gulp';
import rimraf   from 'rimraf';
import sherpa   from 'style-sherpa';
import yaml     from 'js-yaml';
import fs       from 'fs';

// Load all Gulp plugins into one variable
const $ = plugins();

// Check for --production flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { COMPATIBILITY, PORT, UNCSS_OPTIONS, PATHS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
 gulp.series(clean, gulp.parallel(views, sass, javascript, images, copy)));

// Build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', watch));

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  rimraf(PATHS.dist, done);
}

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
function copy() {
  return gulp.src(PATHS.assets)
    .pipe(gulp.dest(PATHS.dist + '/'));
}

// Copy page templates into finished HTML files
function views() {
  return gulp.src('src/views/**/*.{html,hbs,handlebars}')
    .pipe(gulp.dest(PATHS.dist + '/views'));
}

// Compile Sass into CSS
// In production, the CSS is compressed
function sass() {
  return gulp.src(['src/scss/app.scss', 'src/css/*.css'])
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths: PATHS.sass
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: COMPATIBILITY
    }))
    /*.pipe($.if(PRODUCTION, $.uncss(UNCSS_OPTIONS)))
    .pipe($.if(PRODUCTION, $.cssnano()))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))*/
    /*.pipe($.uncss(UNCSS_OPTIONS))*/
    .pipe($.cssnano())
    //.pipe($.sourcemaps.write())
    .pipe($.concat('style.css'))
    .pipe(gulp.dest(PATHS.dist + '/css'));
}

// Combine JavaScript into one file
// In production, the file is minified
function javascript() {
    return gulp.src(PATHS.javascript)
    //.pipe($.sourcemaps.init())
        .pipe($.babel())
        .pipe($.concat('script.js'))
        .pipe($.uglify({mangle: false}))
        /*.pipe($.if(PRODUCTION, $.uglify()
         .on('error', e => { console.log(e); })
         ))*/
        //.pipe($.sourcemaps.write())
        .pipe(gulp.dest(PATHS.dist + '/js'));
}

// Copy images to the "dist" folder
// In production, the images are compressed
function images() {
  return gulp.src('src/img/**/*')
    /*.pipe($.if(PRODUCTION, $.imagemin({
      progressive: true
    })))*/
    .pipe(gulp.dest(PATHS.dist + '/img'));
}

// Watch for changes to static assets, views, Sass, and JavaScript
function watch() {
  gulp.watch(PATHS.assets, gulp.series(copy));
  gulp.watch('src/views/**/*.html', gulp.series(views));
  gulp.watch('src/scss/**/*.scss', sass);
  gulp.watch('src/js/**/*.js', gulp.series(javascript));
  gulp.watch('src/img/**/*', gulp.series(images));
}
