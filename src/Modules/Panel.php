<?php
namespace Modules;

use Slim\Http\Request;
use Slim\Http\Response;

class Panel
{
    function __construct($container)
    {
        $this->container = $container;
    }

    // GET https://domain.com/panel
    function user(Request $request, Response $response)
    {
        $lang = $request->getAttribute('lang');
        switch ($lang)
        {
            case 'en': $lang = '_eng';
                break;
            default: $lang = '_eng';
                break;
        }

        $id = $_SESSION['uid'];

        if (file_exists('../../cache/'.$id.'/borealis_user'.$lang.'.json'))
        {
            $response = $response->withStatus(200)
                ->withHeader('Content-type', 'application/json')
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
                ->write(file_get_contents('../../cache/'.$id.'/borealis_user'.$lang.'.json'));
            return $response;
        }
        else
        {
            try
            {
                $cache = new Cache();
                $person_data = $cache->user($id, $lang, true);

                $response = $response->withStatus(200)
                    ->withHeader('Content-type', 'application/json')
                    ->withHeader('Access-Control-Allow-Origin', '*')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
                    ->write(json_encode($person_data, JSON_FORCE_OBJECT));
                return $response;
            }
            catch (\Exception $e)
            {
                return FALSE;
            }
        }
    }
}