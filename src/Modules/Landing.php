<?php
namespace Modules;

use Slim\Http\Request;
use Slim\Http\Response;
use Core\Cerberus;
use Modules\Registration;

class Landing
{
    // Constructor
    function __construct($container)
    {
        $this->container = $container;
    }

    // Catch non authenticated users
    public function __invoke(Request $request, Response $response, $next)
    {
        if (!isset($_SESSION['authenticated']))
        {
            return $response->withStatus(403)->withHeader('Location', '/');
        }
        $response = $next($request, $response);
        return $response;
    }

    // GET https://domain.com
    function index(Request $request, Response $response)
    {
        //$_SESSION['authenticated'] = 1;
        //$_SESSION['uid'] = 1;
        //$_SESSION['type'] = 1;
        //$_SESSION['active'] = 1;
        //return $this->container->renderer->render($response, '../templates/panel.html');

        if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == 1 && $_SESSION['type'] == 1)
        {
            // Render index view
            return $this->container->renderer->render($response, '../templates/admin.html');
        }
        else if (isset($_SESSION['authenticated']) && $_SESSION['authenticated'] == 1 && $_SESSION['type'] == 2)
        {
            // Render index view
            return $this->container->renderer->render($response, '../templates/panel.html');
        }
        else
        {
            // Render index view
            return $this->container->renderer->render($response, '../templates/landing.html');
        }
    }

    // POST https://domain.com/authenticate
    function authenticate(Request $request, Response $response)
    {
        $data = $request->getParsedBody();

        $cerberus = new Cerberus();

        if ($cerberus->authenticate($data))
        {
            $response = $response->withStatus(200)
                ->withHeader('Content-type', 'application/json')
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
                ->write(201);
            return $response;
        }
        else
        {
            $response = $response->withStatus(200)
                ->withHeader('Content-type', 'application/json')
                ->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
                ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
                ->write(206);
            return $response;
        }
    }

    // ANY https://domain.com/register
    function register(Request $request)
    {
        $data = $request->getParsedBody();

        $data['lang'] = $_COOKIE['NG_TRANSLATE_LANG_KEY'];

        $candidate = new Registration();
        $result = $candidate->processPartner($data);

        return '{"status": '.$result.' }';

        $secret = "6LeUSCETAAAAAKh74sITcImL8t_DM0uEZSnxdkbg";
        $response = null;

        $recaptcha = new \ReCaptcha\ReCaptcha($secret);
        $resp = $recaptcha->verify($data['recaptcha'], $_SERVER['REMOTE_ADDR']);
        if ($resp->isSuccess())
        {
            $candidate = new Registration();
            $result = $candidate->processPartner($data);

            return '{"status": '.$result.' }';
        }
        else
        {
            // Captcha error
            return '{"status": 301 }';
        }
    }

    // GET https://domain.com/activate
    function activate(Response $response)
    {
        if (isset($_GET['username']) && isset($_GET['id']))
        {
            $data['username'] = $_GET['username'];
            $data['hash'] = $_GET['id'];

            $candidate = new Registration();
            $result = $candidate->activateForeigners($data);

            if ($result == 205)
            {
                return $this->container->renderer->render($response, '../templates/activation.html');
            }
            else
            {
                return $this->container->renderer->render($response, '../templates/landing.html');
            }
        }
        else
        {
            return $this->container->renderer->render($response, '../templates/landing.html');
        }
    }

    // POST https://domain.com/reset
    function reset(Request $request)
    {
        $data = $request->getParsedBody();

        $cerberus = new Cerberus();

        if ($cerberus->authenticate($data))
        {
            return '{"status": 1 }';
        }
        else
        {
            return '{"status": 0 }';
        }
    }

    // GET https://domain.com/logout
    function logout(Request $request, Response $response)
    {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 420000, $params['path'], $params['domain'], $params['secure'], $params['httponly']);
        unset($_SESSION);
        session_regenerate_id(TRUE);
        if(session_id())
        {
            session_destroy();
        }
        session_write_close();
        
        return $this->container->renderer->render($response, '../templates/landing.html');
    }
}