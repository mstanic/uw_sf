<?php
namespace Modules;

use Slim\Http\Request;
use Slim\Http\Response;

class Admin
{
    function __construct($container)
    {
        $this->container = $container;
    }

    // POST https://domain.com/admin/submit
    function submit(Request $request, Response $response)
    {
        $lang = $request->getAttribute('lang');
        switch ($lang)
        {
            case 'en': $lang = '_eng';
                break;
            default: $lang = '_eng';
                break;
        }

        $id = $_SESSION['uid'];

        $db = getDB();
        $stmt = $db->query("SELECT * FROM opportunities ORDER BY id ASC");
        $opportunities = $stmt->fetchAll(); //PDO::FETCH_OBJ
        $db = null;

        $opportunities = json_encode(array('opportunities' => $opportunities));

        $response = $response->withStatus(200)
            ->withHeader('Content-type', 'application/json')
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
            ->write(json_encode($opportunities, JSON_FORCE_OBJECT));
        return $response;
    }

    // GET https://domain.com/admin/load
    function load(Request $request, Response $response)
    {
        $lang = $request->getAttribute('lang');
        switch ($lang)
        {
            case 'en': $lang = '_eng';
                break;
            default: $lang = '_eng';
                break;
        }

        $id = $_SESSION['uid'];

        $db = getDB();
        $stmt = $db->query("SELECT * FROM opportunities ORDER BY id ASC");
        $opportunities = $stmt->fetchAll(); //PDO::FETCH_OBJ
        $db = null;

        $opportunities = json_encode(array('opportunities' => $opportunities));

        $response = $response->withStatus(200)
            ->withHeader('Content-type', 'application/json')
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With')
            ->write(json_encode($opportunities, JSON_FORCE_OBJECT));
        return $response;
    }
}