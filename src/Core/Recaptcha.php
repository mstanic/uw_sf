<?php

namespace Core;

class Recaptcha
{
    public static function verify($recaptcha_response)
    {
        // Get cURL resource
        $curl = curl_init();

        // Set some options - we are passing in a useragent too here
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_USERAGENT => 'cURL',
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => array(
                'secret' => '6LenLAcTAAAAAGZN15E58Pf1BSep4rgR0MYqY-2Y',
                'response' => $recaptcha_response
            )
        ));

        // Send the request & save response to $resp
        $resp = json_decode(curl_exec($curl));

        // Close request to clear up some resources
        curl_close($curl);

        //var_dump($resp);

        return $resp->success;
    }
}