<?php
namespace Core;

/**
 * Class ErrorLog
 * @package GW\Core
 */
class ErrorLog
{
    /**
     * Write error details.
     * @param $code
     * @param $pid
     * @param $message
     */
    public static function log($code, $pid, $message)
    {
        $db = getDB();
        $session = (isset($_SESSION['session'])) ? $_SESSION['session'] : '0';
        $query  = "INSERT INTO error_log (code, pid, session, message, timestamp) VALUES ({$code}, {$pid}, '{$session}', '{$message}', now())";
        $db->exec($query);
    }
}