<?php
namespace Core;

use Exception;
use Respect\Validation\Validator as v;

/**
 * Class Cerberus
 * @package Core
 */
class Cerberus
{
    private $db;

    /**
     * Counstructor
     */
    public function __construct()
    {
        $this->db = getDB();
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // AUTHENTICATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Check if the entered username and password match the ones stored in the database.
     * @param Array $payload
     * @return bool TRUE on success, FALSE on failure
     */
    function authenticate($payload = NULL)
    {
        try
        {
            $username = $payload['username'];
            $password = $payload['password'];

            /*$usernameValid = v::email()->notEmpty()->noWhitespace()->length(null, 250)->validate($username);
            $passwordValid = v::stringType()->notEmpty()->alnum()->length(128, 128)->validate($password);

            if ($usernameValid === false || $passwordValid === false)
            {
                return FALSE;
            }*/

            $db = getDB();
            $stmt = $db->prepare("SELECT id, password, salt, type FROM partners WHERE active = 1 AND username = :username");
            $stmt->bindParam(':username', $username);
            $stmt->execute();

            if ($stmt->rowCount() > 0)
            {
                $result = $stmt->fetchAll();

                $id = $result[0]['id'];
                $db_password = $result[0]['password'];
                $db_salt = $result[0]['salt'];
                $db_type = $result[0]['type'];

                $salt = '$2y$05$gSvwHy4IeqYVsm2PeNwdZceWj$';

                $password = crypt($password, $db_salt);
                $password = crypt($password, $salt);

                if ($db_password === $password)
                {
                    $session = session_id();
                    $userAgent = $_SERVER['HTTP_USER_AGENT'];

                    $stmt = $db->prepare("INSERT INTO session_log (session, user_agent, timestamp) VALUES (:session, :user_agent, now())");
                    $stmt->bindParam(':session',    $session);
                    $stmt->bindParam(':user_agent', $userAgent);

                    if ($stmt->execute())
                    {
                        $_SESSION['authenticated'] = 1;
                        $_SESSION['id'] = $id;
                        $_SESSION['session'] = $session;
                        $_SESSION['type'] = $db_type;
                        $_SESSION['userAgent'] = $userAgent;
                        return TRUE;
                    }
                    else
                    {
                        return FALSE;
                    }
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {
                return FALSE;
            }
        }
        catch (Exception $error)
        {
            return FALSE;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // REGISTRATION
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Get user ID based on his username and OTP.
     * @return integer|bool user ID on success, FALSE on failure
     */
    function authenticateOTP($payload)
    {
        return $this->getUserByOTP($payload['user'], $payload['otp']);
    }

    /**
     * Get user ID base on his username and OTP.
     * @param string $user user's username
     * @param string $otp user's otp
     * @return integer|bool user ID on success, FALSE on failure
     */
    function getUserByOTP($user, $otp)
    {
        $db = getDB();
        $query = "SELECT id FROM partners WHERE username = '{$user}' AND hash = '{$otp}'";
        $stmt = $db->query($query);
        $result = $stmt->fetchAll();

        if ($result != FALSE)
        {
            return $result[0]['id'];
        }
        return FALSE;
    }

    /**
     * Reset a user's password.
     * @param integer $id user ID
     * @return array|bool JSON containing user and otp, FALSE on failure
     */
    function resetPassword($id)
    {
        if ($this->checkUser($id) === TRUE)
        {
            $nonce = $this->getNonce();
            $this->storeNonce($id, $nonce);
            //sendMail($id);

            $db = getDB();
            $query = "SELECT id, username, hash AS nonce FROM partners WHERE active = 1 AND id = {$id}";
            $stmt = $db->query($query);
            $result = $stmt->fetchAll();

            $username = $result[0]['username'];
            return array("user" => $username, "otp" => $nonce);
        }
        return FALSE;
    }

    /**
     * Calculate new salt, generate new hash based on that salt and store both parameters.
     * @param integer $user user ID
     * @param string $otp OTP
     * @param string $newPassword new password
     * @return bool TRUE on success, FALSE on failure
     */
    function setPassword($user, $otp, $newPassword)
    {
        $id = $this->getUserByOTP($user, $otp);
        if ($id != FALSE)
        {
            $new_salt = $this->getSalt();
            $new_password = $this->getPassword($newPassword, $new_salt);
            if ($this->storePassword($id, $new_password, $new_salt)) return TRUE;
            return FALSE;
        }
        return FALSE;
    }

    /**
     * Generate new blowfish hashed password.
     * @param string $password SHA3 hash of the new password
     * @param string $salt salt which is paired with the new password
     * @return string blowfish hash of the new password
     */
    function getPassword($password, $salt)
    {
        $password = crypt($password, $salt);
        $password = crypt($password, '$2y$05$gSvwHy4IeqYVsm2PeNwdZceWj$');
        return $password;
    }

    /**
     * Generate new salt.
     * @return string new salt
     */
    function getSalt()
    {
        $i = 1;
        $number = '';
        while ($i < 26)
        {
            $i++;
            $character = chr(rand(33, 125));
            $number .= $character;
        }
        $new_salt = substr(sha1($number), 0, 25);
        $new_salt = '$2y$05$' . $new_salt . '$';
        return $new_salt;
    }

    /**
     * Generate new nonce to be used as OTP.
     * @return string nonce
     */
    function getNonce()
    {
        $i = 1;
        $number = '';
        while ($i < 500)
        {
            $i++;
            $character = chr(rand(33, 125));
            $number .= $character;
        }
        $nonce1 = sha1($number);
        $nonce2 = sha1($nonce1);
        $nonce = $nonce1 . $nonce2;
        return $nonce;
    }

    /**
     * Store user password and salt.
     * @param integer $id user ID
     * @param string $password blowfish hashed password belonging to the user
     * @param string $user_salt salt which is paired with the new password
     * @return bool TRUE on success, FALSE on failure
     */
    function storePassword($id, $password, $user_salt)
    {
        $db = getDB();
        $query = "UPDATE partners SET password = '{$password}', salt = '{$user_salt}', timestamp = now() WHERE active = 1 AND id = {$id}";
        $stmt = $db->query($query);
        $result = $stmt->fetchAll();
        return ($result != FALSE) ? TRUE : FALSE;
    }

    /**
     * Store generated nonce.
     * @param integer $id user ID
     * @param string $nonce nonce which will be stored for the given user
     * @return bool TRUE on success, FALSE on failure
     */
    function storeNonce($id, $nonce)
    {
        $db = getDB();
        $query = "UPDATE partners SET password = NULL, salt = '{$nonce}' WHERE active = 1 AND id = {$id}";
        $stmt = $db->query($query);
        $result = $stmt->fetchAll();
        return ($result != FALSE) ? TRUE : FALSE;
    }

    /**
     * Check if the user with the given ID exists in the database.
     * @param integer $id use-r ID
     * @return bool TRUE on success, FALSE on failure
     */
    function checkUser($id)
    {
        $db = getDB();
        $query = "SELECT id, username, salt FROM partners WHERE active = 1 AND id = {$id}";
        $stmt = $db->query($query);
        $result = $stmt->fetchAll();
        return ($result != FALSE) ? TRUE : FALSE;
    }

    /**
     * Send password reset mail to the user.
     * @param integer $id user ID
     * @return bool|string
     */
    function sendMail($id)
    {
        $db = getDB();
        $query = "SELECT id, username, salt AS nonce FROM partners WHERE active = 1 AND id = {$id}";
        $stmt = $db->query($query);
        $result = $stmt->fetchAll();

        if ($result != FALSE)
        {
            $username = $result[0]['username'];
            $nonce = $this->getNonce();

            //$query = "UPDATE spu.user_credentials SET password = '(null)', salt = '{$nonce}' WHERE active = 1 AND user_credentials.id = {$id}";
            //$result = $db->write($query);

            /*if ($result) {
                $mail = new PHPMailer;

                $mail->isSMTP();
                $mail->Host = 'owa.azvo.hr';
                $mail->SMTPAuth = true;
                $mail->Username = 'spu_mail';
                $mail->Password = 'Pass4spu';
                $mail->SMTPSecure = 'tls';

                $mail->From = 'nispvu@azvo.hr';
                $mail->addAddress('mirko.stanic@fer.hr');
                $mail->addBCC('mirko.stanic@gmail.com');

                $headers = "MIME-Version: 1.0\n";
                $headers .= "Content-type: text/html; charset=utf-8\n";
                $headers .= "Bcc: mirko.stanic@gmail.com\n";
                $headers .= "X-Mailer: ".phpversion();

                $to      = $username;
                $subject = 'Korisnički podaci';
                $message = 'Poštovani,<br><br>'.
                'Središnji prijavni ured izradio je aplikaciju za NISpVU koordinatore koja sadržava potrebne informacije o procesu prijava za upis na studijske programe i aktivnostima koordinatora.<br><br>'.
                'Klikom na priloženi link otvorit će se obrazac za unos lozinke, gdje je potrebno dva puta unijeti lozinku koju ćete sami odabrati:<br><br>'.
                
                'nispvu@azvo.hr<br><br>'.
                'www.studij.hr';
                mail($to, $subject, $message, $headers);

                $mail->Subject = 'Korisnicki podaci';
                $mail->Body    = $message;

                if(!$mail->send()) {
                   echo 'Message could not be sent.';
                   echo 'Mailer ErrorLog: ' . $mail->ErrorInfo;
                   return false;
                }
                return $nonce;
            }*/
            return json_encode(["user" => $username, "otp" => $nonce]);
        }
        return FALSE;
    }
}