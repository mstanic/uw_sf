<?php

// Landing Routes
$app->get('/', 'Modules\Landing:index');

// NISpVU2
$app->post('/authenticate', 'Modules\Landing:authenticate');
$app->any('/register',      'Modules\Landing:register');
$app->get('/activate',      'Modules\Landing:activate');
$app->post('/reset',        'Modules\Landing:reset');
$app->get('/logout',        'Modules\Landing:logout');

// Panel routes
$app->any('/admin/submit', 'Modules\Admin:submit')->add('Modules\Landing');
$app->get('/admin/load',   'Modules\Admin:load')->add('Modules\Landing');

// Panel routes
$app->get('/panel/{lang}', 'Modules\Panel:user')->add('Modules\Landing');

// Catching Landing:index
$app->any('/*', 'Modules\Landing:index');